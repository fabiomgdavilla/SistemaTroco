﻿using System.Collections.Generic;

namespace SistemaTroco.BLL
{
    public class Calculo
    {
        // Retorno simples da subtração
        public static decimal retornoSub(decimal total, decimal pago)
        {
            return pago - total;
        }

        // Retorno da contagem de cédulas e moedas
        public static Dictionary<string, int> retornoTroco(decimal total, decimal pago)
        {
            // Inicialização de dicionário de notas e moedas
            Dictionary<string , int> troco = new Dictionary<string, int>();
            troco.Add("notas de 100", 0);
            troco.Add("notas de 50", 0);
            troco.Add("notas de 10", 0);
            troco.Add("notas de 5", 0);
            troco.Add("notas de 1", 0);
            troco.Add("moedas de 0,50", 0);
            troco.Add("moedas de 0,10", 0);
            troco.Add("moedas de 0,05", 0);
            troco.Add("moedas de 0,01", 0);

            // Armazenamento do valor que será retornado
            decimal result = pago - total;
            // Multiplicação do valor para agilizar a contagem sem ponto flutuante
            result *= 100;

            // Verificação contínua com interrompimento em determinado valor
            while (result > 0)
            {
                if (result >= 10000)
                {
                    result -= 10000;
                    troco["notas de 100"]++;
                    continue;
                }
                if (result >= 5000)
                {
                    result -= 5000;
                    troco["notas de 50"]++;
                    continue;
                }
                if (result >= 1000)
                {
                    result -= 1000;
                    troco["notas de 10"]++;
                    continue;
                }
                if (result >= 500)
                {
                    result -= 500;
                    troco["notas de 5"]++;
                    continue;
                }
                if (result >= 100)
                {
                    result -= 100;
                    troco["notas de 1"]++;
                    continue;
                }
                if (result >= 50)
                {
                    result -= 50;
                    troco["moedas de 0,50"]++;
                    continue;
                }
                if (result >= 10)
                {
                    result -= 10;
                    troco["moedas de 0,10"]++;
                    continue;
                }
                if (result >= 5)
                {
                    result -= 5;
                    troco["moedas de 0,05"]++;
                    continue;
                }
                if (result >= 1)
                {
                    result -= 1;
                    troco["moedas de 0,01"]++;
                    continue;
                }
            }
            // Retorno da contagem
            return troco;
        }
    }
}
