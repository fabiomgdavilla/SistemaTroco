﻿namespace SistemaTroco
{
    partial class TelaInicial
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblValorTotal = new System.Windows.Forms.Label();
            this.lblValorPago = new System.Windows.Forms.Label();
            this.gbValores = new System.Windows.Forms.GroupBox();
            this.numValorPago = new System.Windows.Forms.NumericUpDown();
            this.numValorTotal = new System.Windows.Forms.NumericUpDown();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.gbValores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numValorPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numValorTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // lblValorTotal
            // 
            this.lblValorTotal.AutoSize = true;
            this.lblValorTotal.Location = new System.Drawing.Point(6, 31);
            this.lblValorTotal.Name = "lblValorTotal";
            this.lblValorTotal.Size = new System.Drawing.Size(61, 13);
            this.lblValorTotal.TabIndex = 0;
            this.lblValorTotal.Text = "Valor Total:";
            // 
            // lblValorPago
            // 
            this.lblValorPago.AutoSize = true;
            this.lblValorPago.Location = new System.Drawing.Point(6, 80);
            this.lblValorPago.Name = "lblValorPago";
            this.lblValorPago.Size = new System.Drawing.Size(62, 13);
            this.lblValorPago.TabIndex = 0;
            this.lblValorPago.Text = "Valor Pago:";
            // 
            // gbValores
            // 
            this.gbValores.Controls.Add(this.numValorPago);
            this.gbValores.Controls.Add(this.numValorTotal);
            this.gbValores.Controls.Add(this.lblValorTotal);
            this.gbValores.Controls.Add(this.lblValorPago);
            this.gbValores.Location = new System.Drawing.Point(12, 12);
            this.gbValores.Name = "gbValores";
            this.gbValores.Size = new System.Drawing.Size(259, 124);
            this.gbValores.TabIndex = 0;
            this.gbValores.TabStop = false;
            this.gbValores.Text = "Valores";
            // 
            // numValorPago
            // 
            this.numValorPago.Controls[0].Visible = false;
            this.numValorPago.DecimalPlaces = 2;
            this.numValorPago.Location = new System.Drawing.Point(90, 78);
            this.numValorPago.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numValorPago.Name = "numValorPago";
            this.numValorPago.Size = new System.Drawing.Size(159, 20);
            this.numValorPago.TabIndex = 2;
            // 
            // numValorTotal
            // 
            this.numValorTotal.Controls[0].Visible = false;
            this.numValorTotal.DecimalPlaces = 2;
            this.numValorTotal.Location = new System.Drawing.Point(90, 29);
            this.numValorTotal.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numValorTotal.Name = "numValorTotal";
            this.numValorTotal.Size = new System.Drawing.Size(159, 20);
            this.numValorTotal.TabIndex = 1;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(186, 142);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 3;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // TelaInicial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(283, 176);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.gbValores);
            this.MaximizeBox = false;
            this.Name = "TelaInicial";
            this.Text = "Troco";
            this.gbValores.ResumeLayout(false);
            this.gbValores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numValorPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numValorTotal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblValorTotal;
        private System.Windows.Forms.Label lblValorPago;
        private System.Windows.Forms.GroupBox gbValores;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.NumericUpDown numValorTotal;
        private System.Windows.Forms.NumericUpDown numValorPago;
    }
}

