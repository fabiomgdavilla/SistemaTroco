﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SistemaTroco.BLL;

namespace SistemaTroco
{
    public partial class TelaInicial : Form
    {
        public TelaInicial()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            // Se os campos não são preenchidos
            if (numValorTotal.Value == 0 && numValorPago.Value == 0)
            {
                MessageBox.Show("Insira os valores para a contagem", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            // Se o valor pago não for especificado
            else if (numValorTotal.Value > 0 && numValorPago.Value == 0)
            {
                MessageBox.Show("Insira o valor do pagamento para a contagem", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (numValorTotal.Value == 0 && numValorPago.Value > 0)
            {
                MessageBox.Show("Insira o valor da compra para a contagem", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            // Se os campos estão preenchidos igualmente
            else if ((numValorTotal.Value > 0 && numValorPago.Value > 0) && (numValorTotal.Value == numValorPago.Value))
            {
                MessageBox.Show("Não há troco", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            // Se o total for maior que o valor pago
            else if ((numValorTotal.Value > 0 && numValorPago.Value > 0) && (numValorTotal.Value > numValorPago.Value))
            {
                MessageBox.Show("Valor pago insuficiente!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            // Verificação de valores
            else
            {
                // String de escrita do retorno
                string retorno = "Troco: " + Calculo.retornoSub(numValorTotal.Value, numValorPago.Value).ToString() + "\n\n";

                // Utilização do retorno do nome e valor do Dictionary para mostrar o troco em tela
                foreach (KeyValuePair<string, int> troco in Calculo.retornoTroco(numValorTotal.Value, numValorPago.Value))
                {
                    if (troco.Value != 0)
                    {
                        retorno += "\t" + troco.Key + ": \t" + troco.Value + "\n\n";
                    }
                }
                MessageBox.Show(retorno, "Troco", MessageBoxButtons.OK);
            }
        }
    }
}
